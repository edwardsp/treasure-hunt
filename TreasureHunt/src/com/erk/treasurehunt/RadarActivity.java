package com.erk.treasurehunt;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class RadarActivity extends Activity implements LocationListener {
	private static final int TWO_MINUTES = 1000 * 60 * 2;
	
	private TextView chestCountField;
	private TextView latituteField;
	private TextView longitudeField;
	private LocationManager locationManager;
	private Display display;
	private int width;
	private int nButton = 0;
	private static String baseURL = "http://milestone.if.itb.ac.id/pbd/index.php";
	private static String retrieveURL = "?group_id=d09729a4301a2be9f91ec000203f29c4&action=retrieve&latitude=%f&longitude=%f";
	private static String countURL = "?group_id=d09729a4301a2be9f91ec000203f29c4&action=number";
	
	private Location location = null;
	private double latitude;
	private double longitude;

	public static final String GROUP_ID = "d09729a4301a2be9f91ec000203f29c4";
	public static final String EXTRA_CHEST_ID = "chestId";
	public static final String EXTRA_BSSID = "bssid";
	public static final String EXTRA_DISTANCE = "distance";
	public static final String EXTRA_DEGREE = "degree";
	public static final String EXTRA_LATITUDE = "latitude";
	public static final String EXTRA_LONGITUDE = "longitude";
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_radar);
		chestCountField = (TextView) findViewById(R.id.mytext);
		latituteField = (TextView) findViewById(R.id.textView3);
		longitudeField = (TextView) findViewById(R.id.textView4);
		// Get the location manager
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		// Define the criteria how to select the location provider -> use
		// default
		display = getWindowManager().getDefaultDisplay();
		width = display.getWidth();
	}

	@Override
	protected void onResume() {
		super.onResume();
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60000, 1, this);
		retrieveChest();
	}

	/* Remove the locationlistener updates when Activity is paused */
	@Override
	protected void onPause() {
		super.onPause();
		locationManager.removeUpdates(this);
	}

	@Override
	public void onLocationChanged(Location location) {
		if (isBetterLocation(location, this.location)) {
			this.location = location;
			latitude = location.getLatitude();
			longitude = location.getLongitude();
			latituteField.setText(String.valueOf(latitude));
			longitudeField.setText(String.valueOf(longitude));
		}
	}
	
	protected boolean isBetterLocation(Location location,
			Location currentBestLocation) {
		if (currentBestLocation == null) {
			// A new location is always better than no location
			return true;
		}

		// Check whether the new location fix is newer or older
		long timeDelta = location.getTime() - currentBestLocation.getTime();
		boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
		boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
		boolean isNewer = timeDelta > 0;

		// If it's been more than two minutes since the current location, use
		// the new location
		// because the user has likely moved
		if (isSignificantlyNewer) {
			return true;
			// If the new location is more than two minutes older, it must be
			// worse
		} else if (isSignificantlyOlder) {
			return false;
		}

		// Check whether the new location fix is more or less accurate
		int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation
				.getAccuracy());
		boolean isLessAccurate = accuracyDelta > 0;
		boolean isMoreAccurate = accuracyDelta < 0;
		boolean isSignificantlyLessAccurate = accuracyDelta > 200;

		// Check if the old and new location are from the same provider
		boolean isFromSameProvider = isSameProvider(location.getProvider(),
				currentBestLocation.getProvider());

		// Determine location quality using a combination of timeliness and
		// accuracy
		if (isMoreAccurate) {
			return true;
		} else if (isNewer && !isLessAccurate) {
			return true;
		} else if (isNewer && !isSignificantlyLessAccurate
				&& isFromSameProvider) {
			return true;
		}
		return false;
	}
	
	private boolean isSameProvider(String provider1, String provider2) {
		if (provider1 == null) {
			return provider2 == null;
		}
		return provider1.equals(provider2);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	@Override
	public void onProviderEnabled(String provider) {
		Toast.makeText(this, "Enabled new provider " + provider,
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onProviderDisabled(String provider) {
		Toast.makeText(this, "Disabled provider " + provider,
				Toast.LENGTH_SHORT).show();
	}

	public void refreshHandler(View view) {
		retrieveChest();
	}

	public void retrieveChest() {
		ViewGroup layout = (ViewGroup) findViewById(R.id.radar_layout);
		while (nButton > 0) {
			View btn = layout.findViewById(nButton - 1);
			layout.removeView(btn);
			nButton--;
		}
		if (location != null) {
			String URL = String.format(baseURL+retrieveURL, latitude, longitude);
			ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
			if (networkInfo != null && networkInfo.isConnected()) {
				new chestCountTask().execute(baseURL+countURL);
				new RetrieveChestTask().execute(URL);
			} else {
				Toast.makeText(this, "No network connection available.", Toast.LENGTH_SHORT).show();
			}
		}
		else {
			Toast.makeText(this, "Still trying to get location...", Toast.LENGTH_SHORT).show();
		}
	}

	private class RetrieveChestTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... urls) {
			try {
				return downloadUrl(urls[0]);
			} catch (IOException e) {
				return "Unable to retrieve web page. URL may be invalid.";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			//textView.setText(result);
			try {
				JSONObject json = new JSONObject(result);
				if (json.getString("status").equals("success")) {
					JSONArray chest_array = json.getJSONArray("data");
					if (chest_array != null) {
						int len = chest_array.length();
						nButton = len;
						for (int i = 0; i < len; i++) {
							JSONObject temp = (JSONObject) chest_array.get(i);
							final String id = temp.getString("id");
							final String bssid = temp.getString("bssid");
							final double degree = Double.parseDouble(temp.getString("degree"));
							final double distance = Double.parseDouble(temp.getString("distance")); 
							int x = (int) ((Math.sin(degree*Math.PI/180) * distance * width / 220));
							int y = (int) ((Math.cos(degree*Math.PI/180) *  distance * width / 220)); 
							Button btn = new Button(RadarActivity.this);
							btn.setId(i);
							btn.setBackgroundResource(R.drawable.button);
							RelativeLayout l = (RelativeLayout) findViewById(R.id.radar_layout);
							RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(20,20);
							lp.leftMargin = (width / 2) + x -10;
							lp.topMargin = (width / 2) - y -10;
							l.addView(btn, lp);
							
							btn.setOnClickListener(new View.OnClickListener() {
							    public void onClick(View v) {
									Intent intent = new Intent(RadarActivity.this, TrackingActivity.class);
									intent.putExtra(EXTRA_CHEST_ID, id);
									intent.putExtra(EXTRA_BSSID, bssid);
									intent.putExtra(EXTRA_DEGREE, degree);
									intent.putExtra(EXTRA_DISTANCE, distance);
									intent.putExtra(EXTRA_LATITUDE, latitude);
									intent.putExtra(EXTRA_LONGITUDE, longitude);		
							        startActivity(intent);
							    }
							});
						}
					} else {
						Toast.makeText(RadarActivity.this, "There's no chest nearby.", Toast.LENGTH_SHORT).show();
					}
				} else {
					Toast.makeText(RadarActivity.this, "Invalid parameters.", Toast.LENGTH_SHORT).show();
				}
			} catch (JSONException e) {
				Toast.makeText(RadarActivity.this, "Invalid server response.", Toast.LENGTH_SHORT).show();
			}
		}

		private String downloadUrl(String myurl) throws IOException {
			InputStream is = null;
			try {
				URL url = new URL(myurl);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setReadTimeout(10000);
				conn.setConnectTimeout(15000);
				conn.setRequestMethod("GET");
				conn.setDoInput(true);
				conn.connect();
				is = conn.getInputStream();
				String contentAsString = readIt(is, 2000);
				return contentAsString;
			} finally {
				if (is != null) {
					is.close();
				}
			}
		}

	}
	
	public void resetHandler(View view) {
		ViewGroup layout = (ViewGroup) findViewById(R.id.radar_layout);
		while (nButton > 0) {
			View btn = layout.findViewById(nButton - 1);
			layout.removeView(btn);
			nButton--;
		}
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			new ResetChestTask().execute(baseURL);
		} else {
			Toast.makeText(this, "No network connection available.", Toast.LENGTH_SHORT).show();
		}
	}
	
	private class ResetChestTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... urls) {
			try {
				return downloadUrl(urls[0]);
			} catch (IOException e) {
				return "Unable to retrieve web page. URL may be invalid.";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			// textView.setText(result);
			try {
				JSONObject json = new JSONObject(result);
				if (json.getString("status").equals("success")) {
					Toast.makeText(RadarActivity.this, "Successfully reseting chests.", Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(RadarActivity.this, "Failed to reset chests.", Toast.LENGTH_SHORT).show();
				}
			} catch (JSONException e) {
				Toast.makeText(RadarActivity.this, "Invalid server response.", Toast.LENGTH_SHORT).show();
			}
		}

		private String downloadUrl(String myurl) throws IOException {
			InputStream is = null;
			try {
				URL url = new URL(myurl);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setReadTimeout(10000);
				conn.setConnectTimeout(15000);
				conn.setRequestMethod("POST");
				conn.setDoInput(true);
				conn.setDoOutput(true);
				OutputStream os = conn.getOutputStream();
				BufferedWriter writer = new BufferedWriter(
				        new OutputStreamWriter(os, "UTF-8"));
				writer.write("group_id=d09729a4301a2be9f91ec000203f29c4&action=reset");
				writer.close();
				os.close();
				conn.connect();
				is = conn.getInputStream();
				String contentAsString = readIt(is, 500);
				return contentAsString;
			} finally {
				if (is != null) {
					is.close();
				}
			}
		}

	}
		
	private class chestCountTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... urls) {
			try {
				return downloadUrl(urls[0]);
			} catch (IOException e) {
				return "Unable to retrieve web page. URL may be invalid.";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			//textView.setText(result);
			try {
				JSONObject json = new JSONObject(result);
				if (json.getString("status").equals("success")) {
					chestCountField.setText("Unachieved chest count : "+json.getString("data"));
				} else {
					Toast.makeText(RadarActivity.this, "Failed to get chest count", Toast.LENGTH_SHORT).show();
				}
			} catch (JSONException e) {
				Toast.makeText(RadarActivity.this, "Invalid server response.", Toast.LENGTH_SHORT).show();
			}
		}

		private String downloadUrl(String myurl) throws IOException {
			InputStream is = null;
			try {
				URL url = new URL(myurl);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setReadTimeout(10000);
				conn.setConnectTimeout(15000);
				conn.setRequestMethod("GET");
				conn.setDoInput(true);
				conn.connect();
				is = conn.getInputStream();
				String contentAsString = readIt(is, 500);
				return contentAsString;
			} finally {
				if (is != null) {
					is.close();
				}
			}
		}

	}
	


	// Reads an InputStream and converts it to a String.
	public String readIt(InputStream stream, int len) throws IOException,
			UnsupportedEncodingException {
		Reader reader = null;
		reader = new InputStreamReader(stream, "UTF-8");
		char[] buffer = new char[len];
		reader.read(buffer);
		return new String(buffer);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.radar, menu);
		return true;
	}
}
