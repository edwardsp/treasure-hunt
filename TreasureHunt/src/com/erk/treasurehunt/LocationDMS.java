package com.erk.treasurehunt;

import android.location.Location;

public class LocationDMS extends Location {

	public LocationDMS(Location l) {
		super(l);
	}

	public String getLatitudeDMS() {
		return convert(this.getLatitude());
	}
	
	/**
	 * returns ref for latitude which is S or N.
	 * 
	 * @param latitude
	 * @return S or N
	 */
	public String getLatitudeRef() {
		return this.getLatitude() < 0.0d ? "S" : "N";
	}

	public String getLongitudeDMS() {
		return convert(this.getLongitude());
	}
	
	/**
	 * returns ref for latitude which is S or N.
	 * 
	 * @param latitude
	 * @return S or N
	 */
	public String getLongitudeRef() {
		return this.getLongitude() < 0.0d ? "W" : "E";
	}

	/**
	 * convert latitude into DMS (degree minute second) format. For instance<br/>
	 * -79.948862 becomes<br/>
	 * 79/1,56/1,55903/1000<br/>
	 * It works for latitude and longitude<br/>
	 * 
	 * @param latitude
	 *            could be longitude.
	 * @return
	 */
	private static final String convert(double latitude) {
		StringBuilder sb = new StringBuilder(20);

		latitude = Math.abs(latitude);
		int degree = (int) latitude;
		latitude *= 60;
		latitude -= (degree * 60.0d);
		int minute = (int) latitude;
		latitude *= 60;
		latitude -= (minute * 60.0d);
		int second = (int) (latitude * 1000.0d);

		sb.setLength(0);
		sb.append(degree);
		sb.append("/1,");
		sb.append(minute);
		sb.append("/1,");
		sb.append(second);
		sb.append("/1000,");
		
		return sb.toString();
	}
}
