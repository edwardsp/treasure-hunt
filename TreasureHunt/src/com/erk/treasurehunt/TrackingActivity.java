package com.erk.treasurehunt;

import java.io.IOException;
import java.util.List;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class TrackingActivity extends Activity implements SensorEventListener,
		LocationListener {

	private static final String TAG = "TrackingActivity";

	CompassView compassView;
	private SensorManager sensorManager;
	private Sensor sensor;
	private LocationManager locationManager;
	float volume;
	float sx = 1;
	float prevSx;
	Location target;
	double distance;
	double degree;
	double latitude, longitude;
	String bssid;
	String chestId;
	boolean isFirst = true;
	Location currentLocation;
	float deviasi = 0;
	MediaPlayer mp;
	float dynamicDistance = 0;
	float position;
	
	private WifiManager wifiManager;
	private BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			List<ScanResult> results = wifiManager.getScanResults();

			ScanResult wifi = null;
			for (ScanResult result : results) {
				if (bssid.equals(result.BSSID)) {
					TrackingActivity.this.setTitle(String.format("%s (%d)", result.BSSID, result.level));
					wifi = result;
				}
			}
			
			if (wifi == null) {
				TrackingActivity.this.setTitle(String.format("%s (not found)", bssid));
			}
			wifiManager.startScan();
		}
	};
	
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		compassView = new CompassView(this);

		setContentView(compassView);

		Intent i = getIntent();
		// distance = i.getDoubleExtra(RadarActivity.EXTRA_DISTANCE,
		// 15.650373223621);
		// degree = i.getDoubleExtra(RadarActivity.EXTRA_DEGREE, 245);
		// latitude = i.getDoubleExtra(RadarActivity.EXTRA_LATITUDE, -6.890645);
		// longitude = i.getDoubleExtra(RadarActivity.EXTRA_LONGITUDE,
		// 107.609624);

		distance = i.getDoubleExtra(RadarActivity.EXTRA_DISTANCE, 0);
		degree = i.getDoubleExtra(RadarActivity.EXTRA_DEGREE, 0);
		bssid = i.getStringExtra(RadarActivity.EXTRA_BSSID);
		chestId = i.getStringExtra(RadarActivity.EXTRA_CHEST_ID);
		latitude = i.getDoubleExtra(RadarActivity.EXTRA_LATITUDE, 0);
		longitude = i.getDoubleExtra(RadarActivity.EXTRA_LONGITUDE, 0);

		currentLocation = new Location("currentLocation");
		currentLocation.setLatitude(latitude);
		currentLocation.setLongitude(longitude);

		GeoPoint gp = new GeoPoint(currentLocation).project(degree,
				distance / 1000); // getLocationLatLng(degree, distance / 1000,
									// currentLocation);
		target = new Location("target");
		target.setLatitude(gp.getLatitude());
		target.setLongitude(gp.getLongitude());
		volume = 0;

		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		
		wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_tracking, menu);
		return true;
	}

	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
	}

	@Override
	protected void onPause() {
		super.onPause();

		sensorManager.unregisterListener(this);
		locationManager.removeUpdates(this);
		unregisterReceiver(receiver);
		
		mp.reset();
	}

	@Override
	protected void onResume() {
		super.onResume();

		mp = MediaPlayer.create(this, R.raw.beep);
		mp.setLooping(true);
		mp.setVolume(volume, volume);
		mp.start();

		if (sensor != null) {
			sensorManager.registerListener(this, sensor,
					SensorManager.SENSOR_DELAY_NORMAL);
		} else {
			Toast.makeText(this, "Compass Not Found", Toast.LENGTH_LONG).show();
		}

		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
				0, this);

		animate();

		registerReceiver(receiver, new IntentFilter(
				WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
		wifiManager.startScan();
	}

	@SuppressWarnings("deprecation")
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ORIENTATION) {
			position = event.values[0];

			// Log.d(TAG, String.format("Deviasi - Azimuth: %.8f - %.8f = %.8f",
			// position, deviasi, deviasi - position));

			compassView.updateData(deviasi - position);
		}
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case R.id.checkIn:
			Intent intent = new Intent(TrackingActivity.this,
					CameraActivity.class);
			intent.putExtra(RadarActivity.EXTRA_CHEST_ID, chestId);
			intent.putExtra(RadarActivity.EXTRA_BSSID, bssid);
			intent.putExtra(CameraActivity.EXTRA_CAMERA_LATITUDE,
					currentLocation.getLatitude());
			intent.putExtra(CameraActivity.EXTRA_CAMERA_LONGITUDE,
					currentLocation.getLongitude());
			startActivity(intent);
			break;
		// case R.id.dynamicdistance:
		// Toast.makeText(
		// this,
		// "dynamicDistance: " + dynamicDistance + "   latitutde: "
		// + target.getLatitude(), Toast.LENGTH_SHORT).show();
		// new AlertDialog.Builder(this)
		// .setTitle("Failed")
		// .setMessage(
		// String.format(
		// "Current Location\nLat: %.8f\nLng: %.8f\n\nChest\nLat: %.8f\nLng: %.8f\n\nDistance: %f",
		// currentLocation.getLatitude(),
		// currentLocation.getLongitude(),
		// target.getLatitude(),
		// target.getLongitude(), dynamicDistance))
		// .setPositiveButton("OK", null).show();
		// break;
		// case R.id.mymap:
		// String uri = "geo:" + currentLocation.getLatitude() + ","
		// + currentLocation.getLongitude();
		// startActivity(new Intent(android.content.Intent.ACTION_VIEW,
		// Uri.parse(uri)));
		// break;
		// case R.id.chestmap:
		// String uri2 = "geo:" + target.getLatitude() + ","
		// + target.getLongitude();
		// startActivity(new Intent(android.content.Intent.ACTION_VIEW,
		// Uri.parse(uri2)));
		// break;
		default:
			break;
		}

		return super.onMenuItemSelected(featureId, item);
	}

	private void animate() {
		deviasi = (float) new GeoPoint(currentLocation).bearingTo(new GeoPoint(
				target));// currentLocation.bearingTo(target);

		Log.d(TAG, "Deviasi: " + deviasi);

		dynamicDistance = (float) new GeoPoint(currentLocation)
				.distanceTo(new GeoPoint(target)) * 1000;
		Log.d(TAG, String.format("dynamicDistance: %.8f ", dynamicDistance));

		float scaleFactor;
		if (dynamicDistance <= 100) {
			System.out.println("ATSSSSSSSSSSSSSS");
			volume = 1 - (dynamicDistance / 100);
			// some += 0.2f;
			// sx = 1f * some;
			// compassView.setScaleX(sx / prevSx);
			// compassView.setScaleY(sx / prevSx);
			scaleFactor = 1 - (dynamicDistance / 100);
		} else {
			System.out.println("BWHHHHHHHHHH");
			volume = 0;
			scaleFactor = 0;
		}
		mp.setVolume(volume, volume);
		compassView.scale(scaleFactor);
	}

	public void onLocationChanged(Location location) {
		currentLocation.setLatitude(location.getLatitude());
		currentLocation.setLongitude(location.getLongitude());

		animate();
	}

	public void onProviderDisabled(String provider) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Location Manager");
		builder.setMessage("Would you like to enable GPS?");
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				// Launch settings, allowing user to make a change
				Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				startActivity(i);
			}
		});
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				// No location service, no Activity
				finish();
			}
		});
		builder.create().show();

	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}
}
