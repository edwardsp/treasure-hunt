package com.erk.treasurehunt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.ClientProtocolException;
import ch.boye.httpclientandroidlib.client.HttpClient;
import ch.boye.httpclientandroidlib.client.methods.HttpPost;
import ch.boye.httpclientandroidlib.entity.mime.HttpMultipartMode;
import ch.boye.httpclientandroidlib.entity.mime.MultipartEntity;
import ch.boye.httpclientandroidlib.entity.mime.content.FileBody;
import ch.boye.httpclientandroidlib.entity.mime.content.StringBody;
import ch.boye.httpclientandroidlib.impl.client.DefaultHttpClient;

public class CameraActivity extends Activity implements PictureCallback,
		LocationListener {
	private static final int TWO_MINUTES = 1000 * 60 * 2;
	
	public static final String EXTRA_CAMERA_LATITUDE = "cameraLatitude";
	public static final String EXTRA_CAMERA_LONGITUDE = "cameraLongitude";

	private String chestId;
	private String bssid;
	private int wifiLevel = 0;

	private boolean isScannedWifi = false;
	private boolean isScannedLocation = false;

	private ProgressDialog pd;
	private Button btnCapture;
	private TextView txtLocationLatitude;
	private TextView txtLocationLongitude;
	private TextView txtLocationAccuracy;
	private TextView txtLocationBssid;
	private TextView txtLocationLevel;

	private Camera mCamera;
	private CameraPreview mPreview;
	private FrameLayout framePreview;

	// Acquire a reference to the system Location Manager
	private LocationManager locationManager;
	private WifiManager wifiManager;

	private Location location = null;
	private BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			List<ScanResult> results = wifiManager.getScanResults();

			ScanResult wifi = null;
			for (ScanResult result : results) {
				// Toast.makeText(context, String.format("%s\n%s\n%d",
				// result.BSSID, result.BSSID, result.level),
				// Toast.LENGTH_LONG).show();
				if (bssid.equals(result.BSSID)) {
					wifiLevel = result.level;
					CameraActivity.this.txtLocationLevel.setText(Integer
							.toString(wifiLevel));
					wifi = result;
				}
			}

			if (wifi != null) {
				Toast.makeText(context,
						String.format("BSSID %s (%s) has %d dbM", bssid, wifi.SSID, wifiLevel),
						Toast.LENGTH_LONG).show();
				isScannedWifi = true;
			} else {
				Toast.makeText(context,
						String.format("BSSID %s is not found", bssid),
						Toast.LENGTH_LONG).show();
				CameraActivity.this.txtLocationLevel.setText("not found");
			}

			CameraActivity.this.unregisterReceiver(this);
		}
	};

	private static final String TAG = "CameraActivity";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camera);

		locationManager = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);
		wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);

		btnCapture = (Button) findViewById(R.id.button_capture);
		txtLocationLatitude = (TextView) findViewById(R.id.camera_location_latitude);
		txtLocationLongitude = (TextView) findViewById(R.id.camera_location_longitude);
		txtLocationAccuracy = (TextView) findViewById(R.id.camera_location_accuracy);
		txtLocationBssid = (TextView) findViewById(R.id.camera_location_bssid);
		txtLocationLevel = (TextView) findViewById(R.id.camera_location_level);

		Intent intent = getIntent();
		bssid = intent.getStringExtra(RadarActivity.EXTRA_BSSID);
		chestId = intent.getStringExtra(RadarActivity.EXTRA_CHEST_ID);
		
		if (intent.hasExtra(EXTRA_CAMERA_LATITUDE) && intent.hasExtra(EXTRA_CAMERA_LONGITUDE)) {
			Location location = new Location("gps");
			location.setLatitude(intent.getDoubleExtra(EXTRA_CAMERA_LATITUDE, 0));
			location.setLongitude(intent.getDoubleExtra(EXTRA_CAMERA_LONGITUDE, 0));
			onLocationChanged(location);
		}

		txtLocationBssid.setText(bssid);
	}

	@Override
	public void onPictureTaken(final byte[] data, Camera camera) {
		btnCapture.setEnabled(false);

		AsyncTask<byte[], Void, JSONObject> task = new AsyncTask<byte[], Void, JSONObject>() {
			@Override
			protected void onPreExecute() {
				pd = new ProgressDialog(CameraActivity.this);
				pd.setTitle("Processing...");
				pd.setMessage("Please wait.");
				pd.setCancelable(false);
				pd.setIndeterminate(true);
				pd.show();
			}

			@Override
			protected JSONObject doInBackground(byte[]... arg0) {
				File pictureFile = getOutputMediaFile();
				if (pictureFile == null) {
					Log.d(TAG,
							"Error creating media file, check storage permissions");
					return null;
				}

				try {
					FileOutputStream fos = new FileOutputStream(pictureFile);
					fos.write(arg0[0]);
					fos.close();

					if (location != null) {
						LocationDMS loc = new LocationDMS(location);

						ExifInterface exif = new ExifInterface(
								pictureFile.getAbsolutePath());
						exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE,
								loc.getLatitudeDMS());
						exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF,
								loc.getLatitudeRef());
						exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE,
								loc.getLongitudeDMS());
						exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF,
								loc.getLongitudeRef());
						exif.saveAttributes();
					}
				} catch (FileNotFoundException e) {
					Log.e(TAG, "File not found: " + e.getMessage());
				} catch (IOException e) {
					Log.e(TAG, "Error accessing file: " + e.getMessage());
				}

				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(
						"http://milestone.if.itb.ac.id/pbd/index.php");

				try {
					MultipartEntity reqEntity = new MultipartEntity(
							HttpMultipartMode.BROWSER_COMPATIBLE);
					reqEntity.addPart("group_id", new StringBody(
							RadarActivity.GROUP_ID));
					reqEntity.addPart("chest_id", new StringBody(chestId));
					reqEntity.addPart("file", new FileBody(pictureFile));
					reqEntity.addPart("bssid", new StringBody(bssid));
					reqEntity.addPart("wifi",
							new StringBody(Integer.toString(wifiLevel)));
					reqEntity.addPart("action", new StringBody("acquire"));
					httppost.setEntity(reqEntity);

					// Execute HTTP Post Request
					HttpResponse response = httpclient.execute(httppost);
					BufferedReader br = new BufferedReader(
							new InputStreamReader(response.getEntity()
									.getContent()));
					return new JSONObject(new JSONTokener(br.readLine()));
					// Toast.makeText(this, br.readLine(),
					// Toast.LENGTH_LONG).show();
				} catch (ClientProtocolException e) {
					Log.e(TAG, "Error client protocol : " + e.getMessage());
				} catch (IOException e) {
					Log.e(TAG, "Error accessing file: " + e.getMessage());
				} catch (JSONException e) {
					Log.e(TAG, "Error JSON Parse: " + e.getMessage());
				}

				return null;
			}

			@Override
			protected void onPostExecute(JSONObject result) {
				if (pd != null) {
					pd.dismiss();
					btnCapture.setEnabled(true);
					mCamera.startPreview();
				}

				if (result != null) {
					if ("success".equals(result.getString("status"))) {
						new AlertDialog.Builder(CameraActivity.this)
								.setTitle("Success")
								.setMessage("Acquire chest success.")
								.setPositiveButton("OK",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int whichButton) {
												Intent goToMainActivity = new Intent(
														CameraActivity.this,
														RadarActivity.class);
												goToMainActivity
														.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
												startActivity(goToMainActivity);
											}
										}).show();
					} else {
						new AlertDialog.Builder(CameraActivity.this)
								.setTitle("Failed")
								.setMessage(result.getString("description"))
								.setPositiveButton("OK", null).show();
					}
				} else {
					Toast.makeText(CameraActivity.this, "ERROR JSON",
							Toast.LENGTH_LONG).show();
				}
			}
		};
		task.execute(data);
	}

	@Override
	protected void onResume() {
		super.onResume();

		CameraActivity.this.btnCapture.setEnabled(true);

		if (mCamera == null && (mCamera = Camera.open()) != null) {
			Camera.Parameters params = mCamera.getParameters();
			params.setPictureSize(640, 480);
			mCamera.setParameters(params);

			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, 60000, 1, this);
		}

		// Create our Preview view and set it as the content of our activity.
		mPreview = new CameraPreview(this, mCamera);
		framePreview = (FrameLayout) findViewById(R.id.camera_preview);
		framePreview.addView(mPreview);

		registerReceiver(receiver, new IntentFilter(
				WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
		wifiManager.startScan();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.camera, menu);
		return true;
	}

	@Override
	protected void onPause() {
		super.onPause();

		if (mCamera != null) {
			mCamera.release(); // release the camera for other applications
			mCamera = null;
		}

		framePreview.removeView(mPreview);
		mPreview = null;

		locationManager.removeUpdates(this);
	}

	public void btnCapture_OnClick(View view) {
		boolean isAllowed = isScannedLocation && isScannedWifi;

		if (isAllowed) {
			mCamera.takePicture(null, null, this);
		} else {
			new AlertDialog.Builder(CameraActivity.this)
					.setTitle("Acquire")
					.setMessage(
							"Acquire parameters are not complete. Force upload?")
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
									mCamera.takePicture(null, null,
											CameraActivity.this);
								}
							})
					.setNegativeButton("No", null).show();
		}
	}

	/** Create a File for saving an image or video */
	private File getOutputMediaFile() {
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.

		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				getString(R.string.app_name));

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(TAG, "failed to create directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new java.util.Date());
		File mediaFile = new File(mediaStorageDir.getPath() + File.separator
				+ "IMG_" + timeStamp + ".jpg");

		return mediaFile;
	}

	@Override
	public void onLocationChanged(Location location) {
		if (isBetterLocation(location, this.location)) {
			this.location = location;

			if (mCamera != null) {
				Camera.Parameters params = mCamera.getParameters();
				params.setGpsAltitude(location.getAltitude());
				params.setGpsLatitude(location.getLatitude());
				params.setGpsLongitude(location.getLongitude());
				params.setGpsTimestamp(location.getTime());
				mCamera.setParameters(params);
			}

			txtLocationLatitude.setText(String.format("%3.6f",
					location.getLatitude()));
			txtLocationLongitude.setText(String.format("%3.6f",
					location.getLongitude()));
			txtLocationAccuracy.setText(String.format("%3.6f meter",
					location.getAccuracy()));

			isScannedLocation = true;
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	/**
	 * Determines whether one Location reading is better than the current
	 * Location fix
	 * 
	 * @param location
	 *            The new Location that you want to evaluate
	 * @param currentBestLocation
	 *            The current Location fix, to which you want to compare the new
	 *            one
	 */
	protected boolean isBetterLocation(Location location,
			Location currentBestLocation) {
		if (currentBestLocation == null) {
			// A new location is always better than no location
			return true;
		}

		// Check whether the new location fix is newer or older
		long timeDelta = location.getTime() - currentBestLocation.getTime();
		boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
		boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
		boolean isNewer = timeDelta > 0;

		// If it's been more than two minutes since the current location, use
		// the new location
		// because the user has likely moved
		if (isSignificantlyNewer) {
			return true;
			// If the new location is more than two minutes older, it must be
			// worse
		} else if (isSignificantlyOlder) {
			return false;
		}

		// Check whether the new location fix is more or less accurate
		int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation
				.getAccuracy());
		boolean isLessAccurate = accuracyDelta > 0;
		boolean isMoreAccurate = accuracyDelta < 0;
		boolean isSignificantlyLessAccurate = accuracyDelta > 200;

		// Check if the old and new location are from the same provider
		boolean isFromSameProvider = isSameProvider(location.getProvider(),
				currentBestLocation.getProvider());

		// Determine location quality using a combination of timeliness and
		// accuracy
		if (isMoreAccurate) {
			return true;
		} else if (isNewer && !isLessAccurate) {
			return true;
		} else if (isNewer && !isSignificantlyLessAccurate
				&& isFromSameProvider) {
			return true;
		}
		return false;
	}

	/** Checks whether two providers are the same */
	private boolean isSameProvider(String provider1, String provider2) {
		if (provider1 == null) {
			return provider2 == null;
		}
		return provider1.equals(provider2);
	}
}