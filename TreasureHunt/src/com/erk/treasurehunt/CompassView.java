package com.erk.treasurehunt;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.view.ViewGroup;
import android.widget.ImageView;

public class CompassView extends ImageView {
	public static final int IMAGE_SIZE = 300;
	// private Paint paint;
	private float position = 0;
	Handler handler = new Handler();

	Canvas canvas;
	float sx = 0;

	public CompassView(Context context) {
		super(context);
		this.setImageResource(R.drawable.a);

	}

	@Override
	public void onDraw(Canvas canvas) {
		this.canvas = canvas;
		int height = this.getHeight();
		int width = this.getWidth();
		this.getLayoutParams().height = IMAGE_SIZE;
		this.getLayoutParams().width = IMAGE_SIZE;
		canvas.rotate(position, width / 2, height / 2);

		super.onDraw(canvas);
	}

	public void updateData(float position) {
		this.position = position;
		invalidate();
	}

	public void scale(float sx) {
		// if(canvas != null)
		// canvas.scale(sx, sx);
		if (sx < 0.1f) {
			sx = 0.1f;
		}
		ViewGroup.LayoutParams params = this.getLayoutParams();
		params.height = (int) (IMAGE_SIZE * sx);
		params.width = (int) (IMAGE_SIZE * sx);
		this.setLayoutParams(params);

	}
}
