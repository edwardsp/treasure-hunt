package com.erk.treasurehunt;

import android.location.Location;

/**
 * Abstraction of geographic point. Based on: c:geo - The powerful Android
 * geocaching app. http://www.cgeo.org And MovableType
 */
public class GeoPoint {


	public static final GeoPoint ZERO = new GeoPoint(0.0, 0.0, 0.0);
    private static final double DEG_TO_RAD = Math.PI / 180;
    private static final double RAD_TO_DEG = 180 / Math.PI;
    private static final float EARTH_RADIUS = 6371.0f;
    
    private final double latitude;
    private final double longitude;
    private final double altitude;

    public GeoPoint() {		
    	this(0.0, 0.0, 0.0);
	}
    
    public GeoPoint(Location l) {
		this(l.getLatitude(), l.getLongitude(), l.getAltitude());
	}

    /**
     * Creates new Geopoint with given latitude and longitude (both degree).
     *
     * @param lat latitude
     * @param lon longitude
     */
    public GeoPoint(final double lat, final double lon) {
        this(lat, lon, 0.0);
    }

    /**
     * Creates new Geopoint with given latitude and longitude (both degree).
     *
     * @param lat latitude
     * @param lon longitude
     * @param alt altitude
     */
    public GeoPoint(final double lat, final double lon, final double alt) {
        latitude = lat;
        longitude = lon;
        altitude = alt;
    }
    
    /**
     * Get altitude.
     *
     * @return altitude
     */
    public double getAltitude() {
        return altitude;
    }

    /**
     * Get latitude in degree.
     *
     * @return latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Get latitude in microdegree.
     *
     * @return latitude
     */
    public int getLatitudeE6() {
        return (int) Math.round(latitude * 1E6);
    }

    /**
     * Get longitude in degree.
     *
     * @return longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /*
     * Return a waypoint which is the copy of this one rounded to the given limit.
     * For example, to get a waypoint adapter to a display with 3 digits after the
     * seconds decimal point, a rounding factor of 3600*1000 would be appropriate.
     */
    GeoPoint roundedAt(final long factor) {
        return new GeoPoint(((double) Math.round(latitude * factor)) / factor,
                ((double) Math.round(longitude * factor)) / factor);
    }

    /**
     * Get longitude in microdegree.
     *
     * @return longitude
     */
    public int getLongitudeE6() {
        return (int) Math.round(longitude * 1E6);
    }

    /**
     * Calculates distance to given Geopoint in km.
     *
     * @param point target
     * @return distance in km
     * @throws GeopointException if there is an error in distance calculation
     */
    public double distanceTo(final GeoPoint point) {
        return getDistance(latitude, longitude, point.latitude, point.longitude) / 1000;
    }

    /**
     * Calculates bearing to given Geopoint in degree.
     *
     * @param point target
     * @return bearing in degree, in the [0,360] range
     */
    public double bearingTo(GeoPoint point) {
        double lat1 = Math.toRadians(this.latitude);
        double lat2 = Math.toRadians(point.latitude);
        double dLon = Math.toRadians(point.longitude - this.longitude);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);
        double brng = Math.atan2(y, x);

        double degree = Math.toDegrees(brng);
        if (degree < 0.0) {
            degree += 360.0;
        }
        return degree;
    }

    /**
     * Calculates final bearing to given Geopoint in degree.
     *
     * @param point target
     * @return bearing in degree, in the [0,360] range
     */
    public double finalBearingTo(GeoPoint point) {
        // get initial bearing from supplied point back to this point...
        double lat1 = Math.toRadians(point.latitude);
        double lat2 = Math.toRadians(this.latitude);
        double dLon = Math.toRadians(this.longitude - point.longitude);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2)
                - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);
        double brng = Math.atan2(y, x);

        double degree = Math.toDegrees(brng);
        if (degree < 0.0) {
            degree += 360.0;
        }
        return degree;
    }

    /**
     * Calculates geopoint from given bearing and distance.
     *
     * @param bearing bearing in degree
     * @param distance distance in km
     * @return the projected geopoint
     */
    public GeoPoint project(final double bearing, final double distance) {
        final double rlat1 = latitude * DEG_TO_RAD;
        final double rlon1 = longitude * DEG_TO_RAD;
        final double rbearing = bearing * DEG_TO_RAD;
        final double rdistance = distance / EARTH_RADIUS;

        final double rlat = Math.asin(Math.sin(rlat1) * Math.cos(rdistance) + Math.cos(rlat1) * Math.sin(rdistance) * Math.cos(rbearing));
        final double rlon = rlon1 + Math.atan2(Math.sin(rbearing) * Math.sin(rdistance) * Math.cos(rlat1), Math.cos(rdistance) - Math.sin(rlat1) * Math.sin(rlat));

        return new GeoPoint(rlat * RAD_TO_DEG, rlon * RAD_TO_DEG);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof GeoPoint)) {
            return false;
        }

        final GeoPoint gp = (GeoPoint) obj;
        return getLatitudeE6() == gp.getLatitudeE6() && getLongitudeE6() == gp.getLongitudeE6();
    }

    @Override
    public int hashCode() {
        return getLatitudeE6() ^ getLongitudeE6();
    }

    /**
     * Checks if given Geopoint is similar to this Geopoint with tolerance.
     *
     * @param gp Geopoint to check
     * @param tolerance tolerance in km
     * @return true if similar, false otherwise
     */
    public boolean isEqualTo(GeoPoint gp, double tolerance) {
        return null != gp && distanceTo(gp) <= tolerance;
    }

    /**
     * Returns formatted coordinates with default format.
     *
     * @return formatted coordinates
     */
    @Override
    public String toString() {
        // return "GeoPoint{" + "latitude=" + latitude + ", longitude=" + longitude + ", altitude=" + altitude + '}';
        return String.format("(%.5f, %.5f)", latitude, longitude);
    }

    /**
     * Gets distance in meters (workaround for 4.2.1 JIT bug).
     */
    public static double getDistance(double lat1, double lon1, double lat2, double lon2) {
        double earthRadius = 6372.8; // for haversine use R = 6372.8 km instead of 6371 km
        double dLat = toRadians(lat2 - lat1);
        double dLon = toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(toRadians(lat1)) * Math.cos(toRadians(lat2))
                * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        //double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        //return R * c * 1000;
        // simplify haversine:
        return (2 * earthRadius * 1000 * Math.asin(Math.sqrt(a)));
    }

    public double crossTrackDistance(GeoPoint startPoint, GeoPoint endPoint) {
        double d13 = startPoint.distanceTo(this);
        double brng13 = startPoint.bearingTo(this);
        double brng12 = startPoint.bearingTo(endPoint);
        return Math.abs(Math.asin(Math.sin(d13 / EARTH_RADIUS) * Math.sin(brng13 - brng12)) * EARTH_RADIUS);
    }

    public GeoPoint perpendicular(GeoPoint startPoint, double startBearing) {
        double d13 = startPoint.distanceTo(this);
        double brng13 = startPoint.bearingTo(this);
        double dpr = d13 / EARTH_RADIUS;
        
        double lat = toRadians(latitude);
        double lng = toRadians(longitude);
        
        double pLatitude = toDegrees(Math.asin(Math.sin(lat) * Math.cos(dpr) + Math.cos(lat) * Math.sin(dpr) * Math.cos(brng13)));
        double pLongitude = toDegrees(lng + Math.asin(Math.sin(brng13) * Math.sin(dpr) / Math.cos(toRadians(pLatitude))));
        return new GeoPoint(pLatitude, pLongitude);
    }

    private static double toRadians(double angdeg) {
        return angdeg * DEG_TO_RAD;
    }
    
    private static double toDegrees(double angrad) {
        return angrad * RAD_TO_DEG;
    }

}